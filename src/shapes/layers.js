import PropTypes from 'prop-types'

import { Layer } from '../models'

export const layer = PropTypes.instanceOf(Layer).isRequired
export const layers = PropTypes.objectOf(layer).isRequired
