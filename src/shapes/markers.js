import PropTypes from 'prop-types'

import { Marker } from '../models'

export const marker = PropTypes.instanceOf(Marker).isRequired
export const markers = PropTypes.objectOf(marker).isRequired
