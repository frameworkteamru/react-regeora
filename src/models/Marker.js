import L from 'leaflet'

import { getMaxKey, onMarkerClick } from '../utils'
import { getMarkers, getGroupById } from '../selectors'
import { addMarker, removeMarker } from '../actions'

export class Marker
{
  constructor({dispatch, getState, id, latLng, layer_id})
  {
    this.dispatch = dispatch
    this.getState = getState

    this.id = id ? id : getMaxKey(getMarkers(getState())) + 1
    this.latLng = latLng ? latLng : []
    this.layer_id = layer_id ? layer_id : -1
    return this
  }
  initOnMap() {
    if (this.layer_id >= 0) {
      this.mapMarker = L.marker(this.latLng).on('click', onMarkerClick(this))
      getGroupById(this.layer_id)(this.getState()).addLayer(this.mapMarker)
    }
    return this
  }
  pushToState() {
    this.dispatch(addMarker(this))
    return this
  }
  destroy() {
    getGroupById(this.layer_id)(this.getState()).removeLayer(this.mapMarker)
    this.mapMarker.remove()
    this.dispatch(removeMarker(this))
    return this
  }
}
