import L from 'leaflet'

import { getMaxKey } from '../utils'
import { getLayers, getMarkers } from '../selectors'
import { createMarker } from '../actions'

import {
  addLayer,
  removeLayer,
  setActive,
  editLayer,
  saveLayer
} from '../actions'

export class Layer
{
  constructor({dispatch, getState, id, text, markers, group})
  {
    this.dispatch = dispatch
    this.getState = getState

    this.id = id ? id : getMaxKey(getLayers(getState())) + 1
    this.text = text ? text : ''
    this.markers = markers ? markers : []
    this.group = group ? group : L.layerGroup()
    return this
  }
  pushToState() {
    this.dispatch(addLayer(this))
    return this
  }
  makeActive() {
    this.dispatch(setActive(this.id))
    return this
  }
  edit() {
    this.dispatch(editLayer(this.id))
    return this
  }
  save(){
    this.dispatch(saveLayer(this.id))
    return this
  }
  addMarker(latLng) {
    this.dispatch(createMarker({latLng, layer_id: this.id}))
    return this
  }
  destroy() {
    this.group.clearLayers()
    let markers = getMarkers(this.getState())
    this.markers.forEach((id) => {
      markers[id].destroy()
    })
    this.dispatch(removeLayer(this))
    return this
  }
}
