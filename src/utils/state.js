import { eachObject } from './helpers'
import { Layer } from '../models'

export const removeById = (state, action) => {
  let entitiesById = {}
  eachObject(state, (item, key) => {
    if ((+key) === (+action.payload.id)) {
      return null
    }
    entitiesById[key] = state[key]
  })
  return entitiesById
}

export const layerWithMarker = (state, action) => {
  let id = action.payload.id
  let layerId = action.payload.layer_id
  let layer = new Layer({...state[layerId]})
  layer.markers.push(id)
  return layer

}

export const layerWithoutMarker = (state, action) => {
  let id = action.payload.id
  let layerId = action.payload.layer_id
  let markers = state[layerId].markers.filter((marker) => id !== marker)
  let layer = new Layer({...state[layerId], markers})
  return layer
}
