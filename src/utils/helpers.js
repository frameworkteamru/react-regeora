export const mapObject = (object, callback) => Object.keys(object).map((key, index) => callback(object[key], key, index))
export const eachObject = (object, callback) => Object.keys(object).forEach((key, index) => callback(object[key], key, index))

export const notEmptyObject = (object) => Object.keys(object).length > 0
export const emptyObject = (object) => !notEmptyObject(object)

export const getMaxKey = (object) => Math.max.apply(null, Object.keys(object))
