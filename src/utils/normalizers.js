import { normalize, denormalize, schema } from 'normalizr'

const marker = new schema.Entity('markers')

const layer = new schema.Entity('layers', {
  markers: [ marker ]
})

const layers = new schema.Array(layer)

export const normalizeLayers = (json) => normalize(json, layers)

export const denormalizeLayers = (keys, entities) => denormalize(keys, layers, entities)
