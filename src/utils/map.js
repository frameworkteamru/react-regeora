import L from 'leaflet'

import {
  getActive,
  getActiveLayer,
  getMarkers
} from '../selectors'
import { eachObject } from './'

export const initMap = () => L.map('mapid').setView([51.505, -0.09], 13)

export const onMarkerClick = (marker) => ((e) => {
  marker.destroy()
})

export const createMarker = (getState) => ((e) => {
  let state = getState()
  let latLng = [e.latlng.lat, e.latlng.lng]
  let id = getActive(state)
  if (id >= 0) {
    getActiveLayer(state).addMarker(latLng)
  }
})

export const onMapClick = (getState) => ((e) => {
  let classList = e.originalEvent.target.classList
  if (!classList.contains('leaflet-marker-icon')) {
    createMarker(getState)(e)
  }
})

export const initMapMarkers = (getState) => {
  let state = getState()
  let markers = getMarkers(state)
  eachObject(markers, (marker) => {
    marker.initOnMap()
  })
}

export const setTile = (map) => {
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
      '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
  }).addTo(map)
}
