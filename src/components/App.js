import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Map from '../containers/Map'
import Modal from '../containers/Modal'
import Layers from '../containers/Layers'
import '../App.css'

class App extends Component
{
  static propTypes = {
    show: PropTypes.bool.isRequired,
    hideModal: PropTypes.func.isRequired
  }
  render() {
    return (
      <div
        className="row"
        onKeyUp={this.onKeyUp}
      >
        <Modal></Modal>
        <Map className="col-md-6"></Map>
        <Layers className="col-md-6"></Layers>
      </div>
    );
  }
  onKeyUp = (e) => {
    if (this.props.show && e.key === 'Escape') {
      this.props.hideModal()
    }
  }
}

export default App
