import React, { Component } from 'react'
import PropTypes from 'prop-types'
import icons from 'glyphicons'

import { layer } from '../shapes'

export default class Layer extends Component
{
  static propTypes = {
    layer,
    active: PropTypes.bool.isRequired,
    showModal: PropTypes.func.isRequired
  }
  render() {
    return (
      <div
        onClick={(e) => this.onClick(e)}
        className={'list-group-item' + (this.props.active ? ' active': '')}
        role="group"
        aria-label="..."
      >
        <span className="badge">{this.props.layer.markers.length}</span>
        <span>{this.props.layer.text}</span>
        <div className="btn-group float-right">
          <button
            type="button"
            className="btn btn-info"
          >
            {icons.checkHeavy}
          </button>
          <button
            type="button"
            className="btn btn-warning"
          >
            {icons.pencil}
          </button>
          <button
            type="button"
            className="btn btn-danger"
          >
            {icons.cancel}
          </button>
        </div>
      </div>
    )
  }
  onClick = (e) => {
    let classList = e.target.classList
    if (classList.contains('btn-danger')) {
      this.props.layer.destroy()
    } else if (classList.contains('btn-warning')) {
      this.props.layer.edit()
      this.props.showModal()
    } else if (classList.contains('btn-info')) {
      this.props.layer.save()
    } else {
      this.props.layer.makeActive()
    }
  }
}
