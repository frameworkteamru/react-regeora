import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class AddLayer extends Component
{
  static propTypes = {
    createLayer: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props)
    this.state = {value: ''}
  }
  render() {
    return (
      <form className="form-inline" onSubmit={this.onSubmit}>
        <div className="form-group">
          <div className="input-group">
            <input className="form-control" type="text" value={this.state.value} onChange={this.onChange} />
            <button className="input-group-addon btn btn-default" type="submit">
              Add Layer
            </button>
          </div>
        </div>
      </form>
    )
  }
  onChange = (e) => {
    this.setState({value: e.target.value})
  }
  onSubmit = (e) => {
    e.preventDefault()
    this.props.createLayer(this.state.value)
    this.setState({value: ''})
  }
}
