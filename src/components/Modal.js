import React, { Component } from 'react'
import PropTypes from 'prop-types'

import EditLayer from '../containers/EditLayer'

export default class Modal extends Component
{
  static propTypes = {
    show: PropTypes.bool.isRequired,
    editable: PropTypes.number.isRequired
  }
  render() {
    let content = null
    if (this.props.editable >= 0) {
      content = <EditLayer></EditLayer>
    }
    return (
      <div
        className={this.props.show ? 'modal fade show' : 'modal fade'}
        tabIndex="-1"
        role="dialog"
        aria-hidden={this.props.show ? 'false' : 'true'}
        style={this.props.show ? {display: 'block', paddingRight: '15px'} : {}}
        onClick={this.onClick}
      >
        <div className="modal-dialog" role="document">
          {content}
        </div>
      </div>
    )
  }
  onClick = (e) => {
    if (this.props.show) {
      let p = e.target
      while (p) {
        if (p.classList.contains('modal-content')) {
          return
        } else {
          if (p.classList.contains('modal')) break
          p = p.parentElement
        }
      }
      this.props.hideModal()
    }
  }
}
