import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { layer } from '../shapes'

export default class EditLayer extends Component
{
  static propTypes = {
    layer,
    hideModal: PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {text: props.layer.text}
  }
  render() {
    return (
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Edit Layer</h5>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
            onClick={(e) => this.props.hideModal()}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <form id="edit-layer-form" onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="text-name" className="col-form-label">Text:</label>
              <input
                type="text"
                className="form-control"
                id="text-name"
                value={this.state.text}
                onChange={(e) => this.setState({text: e.target.value})}
              />
            </div>
          </form>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
            onClick={(e) => this.props.hideModal()}
          >
            Close
          </button>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={this.onSubmit}
          >
            Save
          </button>
        </div>
      </div>
    )
  }
  componentWillReceiveProps(nextProps) {
    this.setState({text: nextProps.layer.text})
  }
  onSubmit = (e) => {
    e.preventDefault()
    this.props.updateLayer({layer: this.props.layer, text: this.state.text})
  }
}
