import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Map extends Component
{
  static propTypes = {
    map: PropTypes.object.isRequired,
    createMap: PropTypes.func.isRequired,
    initMarkers: PropTypes.func.isRequired
  }
  render() {
    return (
      <div id="mapid"></div>
    );
  }
  componentWillReceiveProps() {
    this.props.initMarkers()
  }
  componentDidMount() {
    this.props.createMap()
  }
  componentWillUnmount() {
    this.props.map.remove()
  }
}

export default Map
