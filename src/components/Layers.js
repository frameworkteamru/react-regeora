import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Layer from '../containers/Layer'
import AddLayer from '../containers/AddLayer'
import { layers } from '../shapes'
import { notEmptyObject, mapObject } from '../utils'

export default class Layers extends Component
{
  static propTypes = {
    layersLoaded: PropTypes.bool.isRequired,
    layersFetching: PropTypes.bool.isRequired,
    layers,
    active: PropTypes.number.isRequired,
    fetchLayers: PropTypes.func.isRequired,
    initMarkers: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props)
    props.fetchLayers()
  }
  render() {
    let content = null
    if (this.props.layersFetching) {
      content = <button className="list-group-item disabled">Loading</button>
    } else if (this.props.layersLoaded) {
      if (notEmptyObject(this.props.layers)) {
        content = mapObject(this.props.layers, (layer, key, index) => {
          return (
            <Layer
              key={layer.id}
              layer={layer}
              active={this.props.active === layer.id}
            />
          )
        })
      } else {
        content = <button className="list-group-item disabled">Empty</button>
      }
    } else {
      content = <button className="list-group-item disabled">Error</button>
    }

    return (
      <div className="list-group">
        {content}
        <AddLayer></AddLayer>
      </div>
    )
  }
  componentWillReceiveProps() {
    this.props.initMarkers()
  }
}
