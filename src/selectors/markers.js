import { createSelector } from 'reselect'

import { getLayerById } from './layers'

export const getMarkers = (state) => state.entities.markers.data

export const getMarkersForLayerById = (id) => createSelector(
  getMarkers,
  getLayerById(id),
  (markers, layer) => {
    let markersForLayer = {}
    layer.markers.forEach((markerId) => {
      markersForLayer[markerId] = markers[markerId]
    })
    return markersForLayer
  }
)
