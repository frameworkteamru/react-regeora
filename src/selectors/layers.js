import { createSelector } from 'reselect'
import { getModalEditable } from './modal'
import { getActive } from './active'

export const getLayers = (state) => state.entities.layers.data
export const getLayersLoaded = (state) => state.entities.layers.loaded
export const getLayersFetching = (state) => state.entities.layers.fetching

export const getEditableLayer = createSelector(
  getLayers,
  getModalEditable,
  (layers, editable) => layers[editable]
)

export const getLayerById = (id) => createSelector(
  getLayers,
  (layers) => layers[id]
)

export const getGroupById = (id) => createSelector(
  getLayers,
  (layers) => layers[id].group
)

export const getActiveLayer = createSelector(
  getLayers,
  getActive,
  (layers, active) => layers[active]
)

export const getActiveGroup = createSelector(
  getActiveLayer,
  (layer) => layer.group
)
