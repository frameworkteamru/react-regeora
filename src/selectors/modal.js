export const getModalEditable = (state) => state.modal.editable
export const getModalShow = (state) => state.modal.show
