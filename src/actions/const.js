export const LAYERS_ADD = '[LAYERS][ADD]'
export const LAYERS_REMOVE = '[LAYERS][REMOVE]'
export const LAYERS_EDIT = '[LAYERS][EDIT]'
export const LAYERS_UPDATE = '[LAYERS][UPDATE]'
export const LAYERS_SET_ACTIVE = '[LAYERS][SET_ACTIVE]'

export const LAYERS_REQUEST = '[LAYERS][REQUEST]'
export const LAYERS_RECEIVE = '[LAYERS][RECEIVE]'
export const LAYERS_RECEIVE_ERROR = '[LAYERS][RECEIVE_ERROR]'

export const LAYERS_SAVE_REQUEST = '[LAYERS][SAVE_REQUEST]'
export const LAYERS_SAVE_SUCCESS = '[LAYERS][SAVE_SUCCESS]'
export const LAYERS_SAVE_ERROR = '[LAYERS][SAVE_RECEIVE_ERROR]'


export const MARKERS_ADD = '[MARKERS][ADD]'
export const MARKERS_REMOVE = '[MARKERS][REMOVE]'
export const MARKERS_SET = '[MARKERS][SET]'

export const MAP_SET = '[MAP][SET]'
export const MAP_SET_ACTIVE = '[MAP][SET_ACTIVE]'
export const MAP_INIT = '[MAP][INIT]'

export const MODAL_SHOW = '[MODAL][SHOW]'
export const MODAL_HIDE = '[MODAL][HIDE]'
