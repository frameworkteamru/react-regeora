import { Marker } from '../models'
import { MARKERS_ADD, MARKERS_REMOVE, MARKERS_SET } from './const'

export const addMarker = (marker) => {
  return {
    type: MARKERS_ADD,
    payload: marker
  }
}

export const createMarker = ({latLng, layer_id}) => ((dispatch, getState) => {
  let marker = new Marker({latLng, layer_id, dispatch, getState})
  marker.pushToState().initOnMap()
})

export const removeMarker = marker => {
  return {
    type: MARKERS_REMOVE,
    payload: marker
  }
}

export const setMarkers = (markers) => {
  return {
    type: MARKERS_SET,
    payload: markers
  }
}
