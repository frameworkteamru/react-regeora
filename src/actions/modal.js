import {
  MODAL_SHOW,
  MODAL_HIDE
} from './const'

export const showModal = () => {
  return {
    type: MODAL_SHOW,
    payload: null
  }
}

export const hideModal = () => {
  return {
    type: MODAL_HIDE,
    payload: null
  }
}
