import { MAP_SET, MAP_INIT } from './const'
import { setActiveLayer } from './layers'
import {
  initMap,
  setTile,
  onMapClick,
  eachObject
} from '../utils'
import {
  getLayers,
  getLayersLoaded,
  getMap,
  getMapInited,
  getMarkers
} from '../selectors'
import { initMapMarkers } from '../utils/'

export const setMap = (map) => {
  return {
    type: MAP_SET,
    payload: map
  }
}

const MapInit = () => {
  return {
    type: MAP_INIT,
    payload: null
  }
}

export function initMarkers() {
  return function (dispatch, getState) {
    let state = getState()
    if (
      getLayersLoaded(state) &&
      Object.keys(getMap(state)).length > 0 &&
      Object.keys(getMarkers(state)).length > 0 &&
      !getMapInited(state)
    ) {
      initMapMarkers(getState)
      dispatch(MapInit())
    }

    return Promise.resolve()
  }
}

export function createMap(callback) {
  return function (dispatch, getState) {
    var map = initMap()
    setTile(map)
    map.on('click', onMapClick(getState))
    return dispatch(setMap(map))
  }
}

export function setActive(id) {
  return function (dispatch, getState) {
    dispatch(setActiveLayer(id))
    let state = getState()
    let layers = getLayers(state)
    let map = state.map
    eachObject(layers, (layer) => {
      layer.group.remove()
    })
    layers[id].group.addTo(map)
    return Promise.resolve()
  }
}
