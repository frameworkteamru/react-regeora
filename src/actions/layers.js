import fetch from 'cross-fetch'

import { getLayers, getMarkers } from '../selectors'
import { normalizeLayers, denormalizeLayers, eachObject, url } from '../utils'
import { Layer, Marker } from '../models'
import { setMarkers } from './markers'

import {
  LAYERS_ADD,
  LAYERS_REMOVE,
  LAYERS_EDIT,
  LAYERS_SET_ACTIVE,

  LAYERS_REQUEST,
  LAYERS_RECEIVE,
  LAYERS_RECEIVE_ERROR,

  LAYERS_SAVE_REQUEST,
  LAYERS_SAVE_SUCCESS,
  LAYERS_SAVE_ERROR
} from './const'

export const addLayer = (layer) => {
  return {
    type: LAYERS_ADD,
    payload: layer
  }
}

export const createLayer = (text) => ((dispatch, getState) => {
  if (!text.trim()) {
    return
  }
  let layer = new Layer({text, dispatch, getState})
  layer.pushToState()
})

export const updateLayer = ({layer, text}) => ((dispatch, getState) => {
  if (!text.trim()) {
    return
  }
  let newLayer = new Layer({...layer, text, dispatch, getState})
  newLayer.pushToState()
})

export const removeLayer = (layer) => {
  return {
    type: LAYERS_REMOVE,
    payload: layer
  }
}

export const editLayer = (id) => {
  return {
    type: LAYERS_EDIT,
    payload: id
  }
}

export const setActiveLayer = (id) => {
  return {
    type: LAYERS_SET_ACTIVE,
    payload: id
  }
}

const requestLayers = () => {
  return {
    type: LAYERS_REQUEST,
    payload: null
  }
}

const receiveLayers = (json) => {
  return {
    type: LAYERS_RECEIVE,
    payload: json
  }
}

const receiveLayersError = () => {
  return {
    type: LAYERS_RECEIVE_ERROR,
    payload: null
  }
}

export function fetchLayers() {
  return function (dispatch, getState) {
    dispatch(requestLayers())
    return fetch(url(`/storage/layers.json`))
      .then(response => response.json())
      .then(
        json => {
          let normalizedData = normalizeLayers(json)
          let entities = normalizedData.entities
          let layers = {}
          eachObject(entities.layers, (layer, key) => {
            layers[key] = new Layer({...layer, dispatch, getState})
          })
          let markers = {}
          eachObject(entities.markers, (marker, key) => {
            markers[key] = new Marker({...marker, dispatch, getState})
          })

          dispatch(receiveLayers(layers))
          dispatch(setMarkers(markers))
        },
        error => dispatch(receiveLayersError())
      )
  }
}

const saveLayerRequest = (body) => {
  return {
    type: LAYERS_SAVE_REQUEST,
    payload: body
  }
}

const saveLayerSuccess = (json) => {
  return {
    type: LAYERS_SAVE_SUCCESS,
    payload: json
  }
}

const saveLayerError = () => {
  return {
    type: LAYERS_SAVE_ERROR,
    payload: null
  }
}

export function saveLayer(id) {
  return function (dispatch, getState) {
    let state = getState()
    let markers = getMarkers(state)
    let layers = getLayers(state)

    let body = denormalizeLayers([id], {markers, layers})

    dispatch(saveLayerRequest(body[id]))

    // DO ASYNC
    if (false) {
      dispatch(saveLayerSuccess(null))
      dispatch(saveLayerError())
    }
    return Promise.resolve()
  }
}
