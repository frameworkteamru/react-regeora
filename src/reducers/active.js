import {
  LAYERS_SET_ACTIVE,
  LAYERS_REMOVE,
} from '../actions/const'

const active = (state = -1, action) => {
  switch (action.type) {
    case LAYERS_SET_ACTIVE:
      return action.payload
    case LAYERS_REMOVE:
      return (action.payload.id === state) ? -1 : state
    default:
      return state
  }
}

export default active
