import { combineReducers } from 'redux'
import show from './show'
import editable from './editable'

const modal = combineReducers({
  editable,
  show
})

export default modal
