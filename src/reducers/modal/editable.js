import {
  LAYERS_EDIT,
  LAYERS_REMOVE
} from '../../actions/const'

const editable = (state = -1, action) => {
  switch (action.type) {
    case LAYERS_EDIT:
      return action.payload
    case LAYERS_REMOVE:
      return -1
    default:
      return state
  }
}

export default editable
