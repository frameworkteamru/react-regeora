import {
  MODAL_SHOW,
  MODAL_HIDE
} from '../../actions/const'

const show = (state = false, action) => {
  switch (action.type) {
    case MODAL_SHOW:
      return true
    case MODAL_HIDE:
      return false
    default:
      return state
  }
}

export default show
