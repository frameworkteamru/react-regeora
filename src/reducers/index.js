import { combineReducers } from 'redux'

import modal from './modal/'
import active from './active'
import map from './map'
import entities from './entities'
import mapInited from './mapInited'

const rootReducer = combineReducers({
  entities,
  active,
  map,
  modal,
  mapInited
})

export default rootReducer
