import { combineReducers } from 'redux'
import data from './data'
import loaded from './loaded'
import fetching from './fetching'

const layers = combineReducers({
  data,
  loaded,
  fetching
})

export default layers
