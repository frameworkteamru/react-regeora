import {
  LAYERS_REQUEST,
  LAYERS_RECEIVE,
  LAYERS_RECEIVE_ERROR
} from '../../actions/const'

const loaded = (state = false, action) => {
  switch (action.type) {
    case LAYERS_REQUEST:
      return true
    case LAYERS_RECEIVE:
    case LAYERS_RECEIVE_ERROR:
      return false
    default:
      return state
  }
}

export default loaded
