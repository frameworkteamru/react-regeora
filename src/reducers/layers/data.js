import {
  LAYERS_ADD,
  LAYERS_REMOVE,
  LAYERS_RECEIVE,
  LAYERS_UPDATE,
  MARKERS_ADD,
  MARKERS_REMOVE,
} from '../../actions/const'
import { removeById, layerWithMarker, layerWithoutMarker } from '../../utils'

const data = (state = {}, action) => {
  switch (action.type) {
    case LAYERS_ADD:
    case LAYERS_UPDATE:
      return {
        ...state,
        [action.payload.id]: action.payload
      }
    case LAYERS_REMOVE:
      return removeById(state, action)
    case LAYERS_RECEIVE:
      return action.payload
    case MARKERS_ADD:
      return {
        ...state,
        [action.payload.layer_id]: layerWithMarker(state, action)
      }
    case MARKERS_REMOVE:
      return {
        ...state,
        [action.payload.layer_id]: layerWithoutMarker(state, action)
      }
    default:
      return state
  }
}

export default data
