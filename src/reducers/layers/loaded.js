import {
  LAYERS_REQUEST,
  LAYERS_RECEIVE
} from '../../actions/const'

const loaded = (state = false, action) => {
  switch (action.type) {
    case LAYERS_RECEIVE:
      return true
    case LAYERS_REQUEST:
      return false
    default:
      return state
  }
}

export default loaded
