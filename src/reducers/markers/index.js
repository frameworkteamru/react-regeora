import { combineReducers } from 'redux'
import data from './data'

const markers = combineReducers({
  data
})

export default markers
