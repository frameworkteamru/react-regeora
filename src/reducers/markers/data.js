import {
  MARKERS_ADD,
  MARKERS_REMOVE,
  MARKERS_SET
} from '../../actions/const'
import { removeById } from '../../utils'

const data = (state = [], action) => {
  switch (action.type) {
    case MARKERS_ADD:
      return {
        ...state,
        [action.payload.id]: action.payload
      }
    case MARKERS_SET:
      return action.payload
    case MARKERS_REMOVE:
      return removeById(state, action)
    default:
      return state
  }
}

export default data
