import {
  MAP_INIT
} from '../actions/const'

const mapInited = (state = false, action) => {
  switch (action.type) {
    case MAP_INIT:
      return true
    default:
      return state
  }
}

export default mapInited
