import { MAP_SET } from '../actions/const'

const map = (state = {}, action) => {
  switch (action.type) {
    case MAP_SET:
      return action.payload
    default:
      return state
  }
}

export default map
