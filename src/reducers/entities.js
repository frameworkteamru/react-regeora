import { combineReducers } from 'redux'

import layers from './layers/'
import markers from './markers/'

const entities = combineReducers({
  layers,
  markers
})

export default entities
