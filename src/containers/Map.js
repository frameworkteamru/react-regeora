import { connect } from 'react-redux'

import {
  getLayers,
  getActive,
  getMap,
  getLayersLoaded,
  getLayersFetching
} from '../selectors'
import { createMap, initMarkers } from '../actions'
import MapComponent from '../components/Map'

const mapStateToProps = (state, ownProps) => {
  return {
    layersLoaded: getLayersLoaded(state),
    layersFetching: getLayersFetching(state),
    layers: getLayers(state),
    active: getActive(state),
    map: getMap(state)
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    createMap: (callback) => dispatch(createMap(callback)),
    initMarkers: () => dispatch(initMarkers())
  }
}

const Map = connect(
  mapStateToProps,
  mapDispatchToProps
)(MapComponent)

export default Map
