import { connect } from 'react-redux'

import {
  showModal
} from '../actions'
import LayerComponent from '../components/Layer'

const mapStateToProps = (state, ownProps) => {
  return {
    //
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    showModal: () => dispatch(showModal())
  }
}

const Layer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LayerComponent)

export default Layer
