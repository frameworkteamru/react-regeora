import { connect } from 'react-redux'

import { getEditableLayer } from '../selectors'
import { hideModal, updateLayer } from '../actions'
import EditLayerComponent from '../components/EditLayer'

const mapStateToProps = (state, ownProps) => {
  return {
    layer: getEditableLayer(state)
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    hideModal: () => dispatch(hideModal()),
    updateLayer: ({layer, text}) => {
      dispatch(updateLayer({layer, text}))
      dispatch(hideModal())
    }
  }
}

const EditLayer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditLayerComponent)

export default EditLayer
