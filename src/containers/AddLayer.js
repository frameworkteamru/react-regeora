import { connect } from 'react-redux'

import { createLayer } from '../actions'
// import { getLayers } from '../selectors'
import AddLayerComponent from '../components/AddLayer'

const mapStateToProps = (state, ownProps) => {
  return {
    //
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    createLayer: (text) => dispatch(createLayer(text))
  }
}

const AddLayer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddLayerComponent)

export default AddLayer
