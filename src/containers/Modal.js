import { connect } from 'react-redux'

import { hideModal } from '../actions'
import { getModalEditable, getModalShow } from '../selectors'
import ModalComponent from '../components/Modal'

const mapStateToProps = (state, ownProps) => {
  return {
    editable: getModalEditable(state),
    show: getModalShow(state)
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    hideModal: () => dispatch(hideModal())
  }
}

const Modal = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalComponent)

export default Modal
