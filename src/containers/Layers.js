import { connect } from 'react-redux'

import {
  getLayers,
  getActive,
  getLayersLoaded,
  getLayersFetching
} from '../selectors'
import { fetchLayers, initMarkers } from '../actions'
import LayersComponent from '../components/Layers'

const mapStateToProps = (state, ownProps) => {
  return {
    layersLoaded: getLayersLoaded(state),
    layersFetching: getLayersFetching(state),
    layers: getLayers(state),
    active: getActive(state)
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchLayers: () => {
      dispatch(fetchLayers())
    },
    initMarkers: () => dispatch(initMarkers())
  }
}

const Layers = connect(
  mapStateToProps,
  mapDispatchToProps
)(LayersComponent)

export default Layers
