import { connect } from 'react-redux'

import { hideModal } from '../actions'
import { getModalShow } from '../selectors'
import AppComponent from '../components/App'

const mapStateToProps = (state, ownProps) => {
  return {
    show: getModalShow(state)
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    hideModal: () => dispatch(hideModal())
  }
}

const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppComponent)

export default App
